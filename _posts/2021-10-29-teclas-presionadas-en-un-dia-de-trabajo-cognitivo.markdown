---
layout: default
intro: true
title: "Teclas presionadas en un dia de trabajo cognitivo"
date: 2021-10-29 19:12:33 -0300
tag: posts
---

# Teclas presionadas en un dia de trabajo cognitivo

un ejercicio de escritura no-creativa.
trans-ladar una praxis a otro lado, un gesto de movimiento.
las teclas presionadas impresas en formato libro

diseño/tapa/edicion/apoyo @librenauta

# Fotos
![tapa 01]({{site.url}}/rapo-blog/images/01.jpg){:class="img-responsive"}
![tapa 02]({{site.url}}/rapo-blog/images/02.jpg){:class="img-responsive"}
![tapa 03]({{site.url}}/rapo-blog/images/03.jpg){:class="img-responsive"}

<hr>
# Codigos

### registrar cada tecla presionada:
```go
xinput list |
  grep -Po 'id=\K\d+(?=.*slave\s*keyboard)' |
  xargs -P0 -n1 xinput test

awk 'BEGIN{while (("xmodmap -pke" | getline) > 0) k[$2]=$4}
     {print $0 "[" k[$NF] "]"}'
```


### parsear la data cruda:
```go
#!/usr/bin/env ruby

@replace_back_space = ARGV[0] ? true : false

exclude_words = [
  '[XF86AudioRaiseVolume]',
  '[XF86AudioLowerVolume]',
  '[Down]',
  '[KP_Insert]',
  '[KP_Home]',
  '[KP_Enter]',
  '[Left]',
  '[Control_L]',
  '[Shift_L]',
  '[Shift_R]',
  '[Home]',
  '[period]',
  '[Alt_L]',
  '[F10]',
  '[Tab]',
  '[Up]',
  '[Next]',
  '[Prior]',
  '[F11]',
  '[F4]',
  '[F2]',
  '[Escape]',
  '[Right]',
  '[End]',
  '[apostrophe]',
  '[ISO_Level3_Shift]',
  '[Caps_Lock]',
  '[Insert]',
  '[KP_Down]',
  '[KP_End]',
  '[KP_Next]',
  '[KP_Left]',
  '[KP_Add]',
  '[KP_Delete]',
  '[bracketleft]',
  '[bracketright]',
  '[backslash]'
]

def _process_back_space(text)
  return text unless @replace_back_space

  # times appears
  # FIXME: si hay barias "borradas" se mezcla
  times = text.scan(/BackSpace/).length

  return text if times == 0

  # first appear index
  index = text.index '[BackSpace]'
  text[index] = "~#{text[index]}"

  # 3 becasue [a] = 3 chars
  start_position = index - (times * 3)
  text[start_position] = "~#{text[start_position]}"

  text.gsub!("\[BackSpace\]", '')

  text
end

def process_back_space(lines, backspace_count)
  # lines: ['a', 'c', 'a', 'space', 'n', 'o', 'space', '.']
  # backspace_count: 5
  # output: aca ~ no .~
  #require "pry"
  #binding.pry

  insert_index = lines.length - backspace_count - 2
  lines.insert(insert_index, '~')
  lines.insert(lines.length - 1, '~')

  # borramos los '[BackSpace]'
  #backspace_count.times do |t|
  #  lines.delete_at(insert_index + 1 + t)
  #end

  lines = lines.join
  lines = lines.gsub("\[BackSpace\]", '')
  lines
end

text = ''
amounted_lines = []
backspace_count = 0
on_backspace = false

File.foreach("./output_curated.log") do |line|
  line.gsub!("\n", '')
  line.gsub!('[space]', ' ')
  line.gsub!('[comma]', ',')
  line.gsub!('[slash]', '/')
  line.gsub!('[minus]', '-')
  line.gsub!('[semicolon]', ';')
  line.gsub!('[equal]', '=')
  #line.gsub!("\[", '')
  #line.gsub!("\]", '')

  next if exclude_words.include? line

  amounted_lines << line

  if line == '[BackSpace]'
    on_backspace = true
    # contamos cuantas veces borramos
    backspace_count += 1
  end

  if on_backspace && line != '[BackSpace]'
    # terminamos de borrar
    # procesamos el .md
    line = process_back_space(amounted_lines, backspace_count)
    text << line

    amounted_lines = []
    on_backspace = false
    backspace_count = 0

    text = text.gsub('[Return]', "\n")
  end

  #if line != '[Return]'
  #  text << line
  #else
  #  # we process [BackSpace] for .md
  #  puts process_back_space(text)
  #  #puts text
  #  text = ''
  #end
end

puts text
puts ARGV[0]
```
