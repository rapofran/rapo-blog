---
layout: default
intro: true
title: Sobre el cuerpo
date: 2020-10-05 23:22:00 -0300
tag: posts
---

# Sobre el cuerpo

La extensión de la mente en lo físico, mi representación en este plano del universo, mi médium para la reencarnación. Todo eso NO es mi cuerpo.

El cuerpo es la imagen del siglo XXI.
El cuerpo es más que la suma de sus partes, si lo miro holísticamente puedo llegar a ver interrelaciones y ahí es donde encuentro sentido: cuerpos haciendo musica, cuerpos amando, cuerpos moviéndose, cuerpos matando otros cuerpos.
Es la entrada de los placeres, los placeres del cuerpo, el sexo. La experiencia.
Sin cuerpo no hay nada porque el cuerpo es memoria.
