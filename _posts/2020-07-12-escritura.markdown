---
layout: default
intro: true
title: Escritura
date: 2020-07-12 17:01:17 -0300
tag: posts
---

## Escribir

Pensaba en escribir.
El acto (el gesto) de poner en palabras algo de adentro. (Adentro de donde?)
Se exterioriza. Alguien puede leerlo (unx mismx?).
Pero es antes; es por el mero hecho de hacerlo, de sacar de adentro, de alivianar(se).
Hay multiples formas de sacar; pero la tecnologia del lenguaje escrito es, quizas y probablemente,
la mas directa.

"Escribo asi no hay penas" ~> de "Toco asi no hay penas" (musica)
Es una praxis, sacar de adentro.

Entonces sacar, sacar. Y luego que?
Seguir sacando y que entren cosas. Compartir(las). Un ciclo.

`while true { sacar sacar y que entren entren cosas }`

Sabiendo que todas las personas vamos a morir. Todas las personas que conocemos van a morir.
Este mundo va a morir. El universo va a morir.
Y?

Para que mirar a otras personas y comparar? (si todas vamos a morir).
Ahi hay una distancia, una pared, no hay (com)unión. Como unir(nos) entre cuerpos?
Con lenguaje? Compartiendo nuestros textos? Siendo anonimxs? Pienso en el sexo.

Pero por qué y para qué "ser alguien"? pienso en el ser para afuera, en el mostrar,
en ese narcisismo necesario pero rancio, en los mundillos del arte (muerte, también morirán).

No nos alcanza con nosotris?

La otra persona viene a sacar? a quitar? siempre?
Algo hay que perder. Y ese _algo_ no lo sabemos a priori: ahi está la entrega.

Se pierde _algo_ al escribir? Me gusta pensar que si y que se gana lo que entrará.

(leo mucho en este mismo texto la palabra "pienso", ese verbo, pensar: tambien es sacar?)
