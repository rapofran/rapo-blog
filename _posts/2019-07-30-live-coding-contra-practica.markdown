---
layout: default
intro: true
title: Live coding como práctica contra-disciplinar de trabajo cognitivo
date: 2019-07-30 02:46:17 -0300
tag: livecoding
---

# Live coding como práctica contra-disciplinar de trabajo cognitivo

[https://crypt.lol/notice/1785788](https://crypt.lol/notice/1785788) el 07/09/2019 publicaba en el fediverso:

_el livecoding como práctica contra-disciplinar de trabajo alienado:_

_puedo hacer cosas con resultados distintos pero haciendo el mismo esfuerzo fisico:
es la misma silla, el mismo teclado, el cuerpo en el mismo lugar, las piernas
los brazos y las manos en la misma posición._

_casi el mismo trabajo mental, pero sabiendo que el resultado es otro y que no es para nadie._

_si todos o algunos dias puedo hacer esto y pasarlo como horas de trabajo, que cambia? pienso que cambia todo_

Propongo pensar, haciendo un gran recorte vertical a la cotidianidad de las personas que se consideran trabajadoras
cognitivas, al live coding como una posible contra-práctica artistica.

Leyendo y citando a Bifo[0] en La fábrica de la infelicidad (2000):

> Que significa trabajar hoy? Como tendencia, de forma cada vez mas general, el trabajo tiene aspecto físico uniforme:
nos sentamos frente a una pantalla y movemos los dedos sobre un teclado: tecleamos.

> El trabajo se ha vuelto al mismo tiempo mucho mas uniforme desde el punto de vista fisico: ergonómico, y mucho mas
diferenciado, especializado en lo que respecta a los contenidos que se elaboran.
Desde el punto de vista físico no hay diferencia entre un agente de viajes, un operador de una refinería de petróleo y un
escritor de novela negra, en el momento en el que desarrollan su trabajo. Pero, al mismo tiempo, lo contrario también es cierto.
El trabajo se ha convertido en parte de un proceso mental, en la elaboración de signos cargados de saber.

> El trabajo se ha hecho muy especifico y especializado. El abogado y el arquitecto, el informático y el dependiente de supermercado
están ante la misma pantalla y pulsan las mismas teclas, pero ninguno de ellos podría ocupar el puesto del otro, porque el contenido
de su actividad de elaboración es irreductiblemente diferente y no es traducible.
Un obrero quimico y un obrero metalurgico realizan trabajos del todo diferentes desde el punto de vista fisico, pero a un
metalurgico le hacen falta solo unos pocos dias para adquirir el conocimiento operativo del trabajo de un obrero quimico y
viceversa. Cuanto mas se simplifica el trabajo industrial por medio de la maquinaria, mas intercambiable se vuelve.

> Frente al ordenador, conectados a la maquina universal de elaboración y comunicacion, los terminales humanos desarrollan
los mismos movimientos fisicos. Pero cuanto mas se simplifica el trabajo desde el punto de vista fisico, menos intercambiables
se vuelven sus conocimientos, sus capacidades, sus prestaciones. El trabajo digitalizado manipula signos absolutamente
abstractos, pero su funcionamiento recombinante es cada vez mas específico, cada vez mas personalizado, cada vez menos intercambiable.

> Por ello los trabajadores _high tech_ tienden a considerar el trabajo como la parte mas esencial de sus vidas, la parte mas
singular y la mas personalizada. Exactamente al contrario de lo que le sucedía al obrero industrial, para quien las ocho
horas de prestación asalariada eran una especie de muerte temporal de la despertaba solo con la sirena que marcaba el fin
de la jornada.

Empresa y deseo: caímos en la trampa.
La creatividad siempre capturada por los tentáculos de la maquinaria productiva.

Como estamos de lleno en la info-esfera digitalizada, en el 2019 y en el trabajo cognitivo, estas comparaciónes con
el trabajo industrial nos sirven para hacernos preguntas y criticar nuestra actividad cotidiana.

Si al livecoding lo llevamos al núcleo del ejercicio del trabajo, a la unidad de valor hora _freelance_, si
lo hacemos **en el trabajo** que obtenemos? es posible?

Quizas si, hacer un context switch mental y programar ruiditos y colores.
No resolver ningun problema. No resolver nada.

O quizas no, no es posible. Who knows? y si probamos al menos?

Usar el trabajo mental para nosotros, para poner nuestra energía deseante en nosotros, para experimentar algo nuevo.
Para programar nuestros propios algoritmos.

Porque ya que no podemos salir de la silla, no salgamos: programemos codigos de programación que hagan ritmos sonoros y visuales.
Algo-ritmos. **Nuestros algo-ritmos.**

Esto puede tener una problematica: estamos en soledad al momento de realizar la actividad.
Que lindo cuando programamos musica y visuales de a *muchas personas*. Como en las algoraves. Ahí hay un gesto muy poético
y potente que no se si somos realmente conscientes <3 cuerpos afectandose alegremente, potenciandose.
"Nadie sabe lo que pueden los algoritmos" xD

Por eso con [CLiC](https://colectivo-de-livecoders.gitlab.io/) necesitamos construir nuestra propia plataforma
para programar de a varias personas. Es software libre, claro, y utilizamos las herramientas/lenguajes que teniamos a mano: ruby.
Hicimos un editor web para que personas abran un navegador web y sin instalar nada puedan escribir tidalcycles.

Entonces, los cuerpos iguales, haciendo los mismos movimientos pero generando códigos/pensamientos en comunidad, improvisando
con el instrumento QWERTY, con la mente a mil y el cuerpo tambien! Mostrando(nos) el código para aprender entre nos, colectivizar
la singularidad de los conocimientos obtenidos, el freesoftware musical y visual.

Salirnos por momentos de las lógicas que nos disciplinan. Al menos por un rato...


[0] [https://es.wikipedia.org/wiki/Franco_Berardi](https://es.wikipedia.org/wiki/Franco_Berardi)
