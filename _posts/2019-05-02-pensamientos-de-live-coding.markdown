---
layout: default
intro: true
title: "Pensamientos de live coding"
date: 2019-05-01 18:46:17 -0300
tag: livecoding
---

# Pensamientos de Live Coding


Porque y para que intentar definir y delimitar una práctica que de por si es intermedial/transmedial?

Quizas sirve en principio para entender que al segregar de otras disciplinas podemos profundizar en su aprendizaje, al
mirar y observar en un campo mucho mas pequeño y detenernos sobre ciertos aspectos puntuales. Pero pronto caemos en cuenta
de que todo es en vano, todo está deliciosamente interconectado.

Ahora bien, live coding, término anglosajon, al español podria traducirse como código vivo, código _en_ vivo?

Tiene la palabra código: es código de lenguajes de programación. Tiene indefectiblemente un lado tecniCista.
Como lograr huír de eso? no se puede. Haría falta? no creo. Igualmente podemos, quizas, intentar aceptar y amigarnos con esa idea.

También en su etimología contiene "en vivo", en teoría seria una práctica para/frente a una audiencia. Lo performativo..
But _who cares?_. Redefinamos y repensemos audiencia.

Decia transmedial, porque como dice Claudia Kozak
> "podemos recurrir al concepto de transmedial para focalizar en modos
de afectación recíproca y procesual entre medios, lenguajes y tecnologías que desestabilizan a tal punto sus espacios de
origen que darían lugar a una concepción de lo "trans" como transformación y mutación"[0]

Podría ser una tecnopoesía?
> "ya que no apunta a un estado de la tecnologia en particular sino a señalar una relación estrecha entre
la poesía y los medios técnicos que le dan su materialidad específica, asi como el diálogo que la poesía establece con el
entramado tecnológico del que surge"[1].

El código que se ve es lo que genera el resultado sonoro/visual. Son ideas expresadas en
texto.

Muy "contemporáneo" todo...


******

El código es simple texto, palabras, frases, que una (o varias) computadoras traducen y que da como resultado ruiditos y colores (por ahora?).

El gesto de escribir en un teclado, la interfaz es el teclado, el teclado de una computadora.
Aquí no hay interfaces gráficas (GUIs, o si, pero no me detengo en ellas).
Las manos en un teclado, la vista en un editor de texto. ¿No es el mismo escenario que el de las trabajadoras cognitivas programadoras
cada vez mas precarizadas? (hablo en femenino neutro por *personas* en el español)

Es el mismo escenario, pero podríamos pensar que el resultado es otro. No hay un fin utilitarista, claro; no hay problemas a resolver.
Es exactamente todo lo contrario. Acá recide lo emocionante, lo poético, porque podría pensarse como un escapismo de
las trabajadoras cognitivas expuestas constantemente a la precarización laboral. Podría? me gusta pensar que si.
_La doble vida, horas programando alienadas para sobrevivir y luego programando para hacer musica/visuales._ No es hermoso?

Es como la sublevación de las trabajadoras cognitivas que dice Bifo[2], podríamos empezar por repensar la práctica de programar.
La programación como una actividad política. Volver a erotizar(nos) la piel en las actividades colaborativas que hagamos,
como no livecodeando solas sino de a muchas personas.

La proyección del código para mostrar el proceso, el instrumento en constante (re)escritura es algo interesante del manifiesto TOPLAP[3].
"Los algo-ritmos son ideas", es otra.

Ni hablar de que las herramientas y lenguajes utilizados son, o deberian ser, en su totalidad software libre.

Acá tiene otro peso político. El compartir el código fuente de lo que se está produciendo. El elegir herramientas que sean
software libre y el apostar por saberes comunitarios y no corporativos-individuales-hegemónicos. Bueno, todo eso está
en el manifiesto de CLiC[4] también.

Una posible conclusión: el live coding es abarcativo hasta en su definición, no hay uno solo. Es lo cada persona lleva
a cabo, hay tantos como singularidades posibles.


Referencias:

- [0] Tecnopoéticas Argentinas, Claudia Kozak, Caja Negra 2015.
- [1] Ídem.
- [2] La Sublevación, Hekht, Buenos Aires (2014).
- [3] [https://toplap.org/wiki/ManifestoDraft](https://toplap.org/wiki/ManifestoDraft)
- [4] [https://colectivo-de-livecoders.gitlab.io/#que-somos](https://colectivo-de-livecoders.gitlab.io/#que-somos)
